package 
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class SpecificgestureSupport extends Sprite 
	{
		private var  traceField:TextField;
		private var traceFormat:TextFormat;
		public function SpecificgestureSupport() 
		{
			init();
		}
		
		private function init():void 
		{
			setupTextField();
			
			checkGestures();
		}
		
		private function checkGestures():void 
		{
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			if(Multitouch.supportedGestures){
				
				var supportGestures:Vector.<String> = Multitouch.supportedGestures;
				
				for (var i:uint = 0; i < supportGestures.length; i++){
					traceField.appendText(supportGestures[i] + "\n");
				}
			}else{
				traceField.appendText("No soporta GESTURES");
			}
		}
		
		private function setupTextField():void 
		{
			traceFormat = new TextFormat();
			traceFormat.font = '_sans';
			traceFormat.size = 22;
			traceFormat.align = 'center';
			traceFormat.color = '0xff0000';
			
			traceField = new TextField();
			traceField.defaultTextFormat = traceFormat;
			traceField.selectable = false;
			traceField.mouseEnabled = false;
			traceField.width = stage.stageWidth;
			traceField.height = stage.stageHeight;
			addChild(traceField);
		}
		
	}

}
package 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.TransformGestureEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class GesturesZoomDisplayObjetc extends Sprite 
	{
		private var box:Shape;
		
		public function GesturesZoomDisplayObjetc() 
		{
			init();
		}
		
		private function init():void 
		{
			setupBox();
			
			setupTouchEvents();
		}
		
		private function setupTouchEvents():void 
		{
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			stage.addEventListener(TransformGestureEvent.GESTURE_ZOOM, onZoom);
		}
		
		private function onZoom(e:TransformGestureEvent):void 
		
		{
			box.scaleX *= e.scaleX;
			box.scaleY *= e.scaleY;
		}
		
		private function setupBox():void 
		{
			box = new Shape();
			box.graphics.beginFill(0x00ff00);
			//box.x = stage.stageWidth / 2;
			//box.y = stage.stageHeight / 2;
			box.graphics.drawRect(stage.stageWidth / 2 -100, stage.stageHeight / 2-100, 200, 200 );
			box.graphics.endFill();
			addChild(box);
		}
		
	}

}
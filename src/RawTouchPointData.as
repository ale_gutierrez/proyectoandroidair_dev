package 
{
	import flash.display.Sprite;
	import flash.events.TouchEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class RawTouchPointData extends Sprite 
	{
		private var traceFormat:TextFormat;
		private var traceField:TextField;
		
		public function RawTouchPointData() 
		{
			init();
		}
		
		private function init():void 
		{
			setupTextField();
			
			setupTouchEvents();
		}
		
		private function setupTouchEvents():void 
		{
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			stage.addEventListener(TouchEvent.TOUCH_MOVE, touchMove);
			stage.addEventListener(TouchEvent.TOUCH_END, touchEnd);
		}
		
		private function touchEnd(e:TouchEvent):void 
		{
			traceField.text = "";
		}
		
		private function touchMove(e:TouchEvent):void 
		{
			traceField.text = "";
		}
		
		
		private function setupTextField():void 
		{
			traceFormat = new TextFormat();
			traceFormat.bold = true;
			traceFormat.size = 22;
			traceFormat.font = '_sans';
			traceFormat.align = 'right';
			
			traceField = new TextField();
			traceField.defaultTextFormat = traceFormat;
			traceField.selectable = false;
			traceField.mouseEnabled = false;
			traceField.width = stage.stageWidth;
			traceField.height = stage.stageHeight;
			addChild(traceField);
			
			
		}
		
	}

}
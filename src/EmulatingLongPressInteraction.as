package 
{
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.events.TouchEvent;
	import flash.geom.Rectangle;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class EmulatingLongPressInteraction extends Sprite 
	{
		private var box:Sprite;
		private var lpTimer:Timer;
		
		public function EmulatingLongPressInteraction() 
		{
			init();
		}
		
		private function init():void 
		{
			setupTimer();
			setupBox();
			setupTocuchEvents();
		}
		
		private function setupTocuchEvents():void 
		{
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			box.addEventListener(TouchEvent.TOUCH_BEGIN, touchBegin);
			box.addEventListener(TouchEvent.TOUCH_END, touchEnd);
			box.addEventListener(TouchEvent.TOUCH_OUT, touchEnd);
			box.addEventListener(TouchEvent.TOUCH_ROLL_OUT, touchEnd);
			
		}
		
		private function touchEnd(e:TouchEvent):void 
		{
			lpTimer.stop();
			box.startDrag();
			box.scaleX = 1;
			box.scaleY = 1;
			box.alpha = 1;
		}
		
		private function touchBegin(e:TouchEvent):void 
		{
			box.scaleX += 0.1;
			box.scaleY += 0.1;
			box.alpha = 0.8;
			lpTimer.start();
		}
		
		private function setupBox():void 
		{
			box = new Sprite();
			box.graphics.beginFill(0xff5500, 1);
			box.x = stage.stageWidth / 2;
			box.y = stage.stageHeight / 2;
			box.graphics.drawRect( -100, -100, 200, 200);
			box.graphics.endFill();
			addChild(box);
		}
		
		private function setupTimer():void 
		{
			lpTimer = new Timer(1000, 1);
			lpTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timerEnd);
		}
		
		private function timerEnd(e:TimerEvent):void 
		{
			var dragBounds:Rectangle = new Rectangle(box.width / 2, 
													 box.height / 2,
													 stage.stageWidth - box.width, 
													 stage.stageHeight - box.height);
			box.startDrag(true, dragBounds);
		}
		
	}

}
package 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.TouchEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class GestureTouchPoint extends Sprite 
	{
		private var traceField:TextField; 
		private var traceFormat:TextFormat;	
		
		
		// cofniguracion de areo de dibujo
		private var drawArea:Shape;
		private var trackBeginObject:Object;
		
		public function GestureTouchPoint() 
		{
			init();
		}
		
		private function init():void 
		{
			setupTextField();
			
			setupDrawArea();
			
			setupTouchEvents();
		}
		
		private function setupTouchEvents():void 
		{
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			trackBeginObject = new Object();
			stage.addEventListener(TouchEvent.TOUCH_BEGIN, touchBegin);
			stage.addEventListener(TouchEvent.TOUCH_MOVE, touchMove);
			stage.addEventListener(TouchEvent.TOUCH_END, touchEnd);
		}
		
		private function touchEnd(e:TouchEvent):void 
		{
			// T: top, arriba o superior
			// B: Bottom, abajo o inferior
			//L:left, izquierda
			//R:Rigth derecha
			if (e.isPrimaryTouchPoint){
				if(e.stageX>trackBeginObject.x && e.stageY>trackBeginObject.y){
					traceField.text = "Gesture Diagonal: TL->BR";
				}else if (e.stageX < trackBeginObject.x && e.stageY > trackBeginObject.y){
					traceField.text = "Gesture Diagonal: TR->BL";
				}else if (e.stageX < trackBeginObject.x && e.stageY < trackBeginObject.y){
					traceField.text = "Gesture Diagonal: BR->TL";
				}else if (e.stageX > trackBeginObject.x && e.stageY < trackBeginObject.y){
					traceField.text = "Gesture Diagonal: BL->TR";
				}
			}
		}
		
		private function touchMove(e:TouchEvent):void 
		{
			if(e.isPrimaryTouchPoint){
				drawArea.graphics.lineTo(e.stageX, e.stageY);
			}
		}
		
		private function touchBegin(e:TouchEvent):void 
		{
			if (e.isPrimaryTouchPoint){
				drawArea.graphics.clear();
				drawArea.graphics.lineStyle(20, 0xff5511, 0.5);
				trackBeginObject.x  = e.stageX;
				trackBeginObject.y = e.stageY;
				drawArea.graphics.moveTo(e.stageX, e.stageY);
			}
		}
		
		private function setupDrawArea():void 
		{
			drawArea = new Shape();
			addChild(drawArea);
		}
		
		private function setupTextField():void 
		{
			traceFormat = new TextFormat();
			traceFormat.align = "center";
			traceFormat.font = "_sans";
			traceFormat.size = 22;
			traceFormat.color = 0xff5599;
			traceField = new TextField();
			traceField.defaultTextFormat = traceFormat;
			traceField.selectable = false;
			traceField.mouseEnabled = false;
			traceField.width = stage.stageWidth;
			traceField.height = stage.stageHeight;
			addChild(traceField);
		}
		
	}

}
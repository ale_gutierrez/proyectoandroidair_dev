package 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.TransformGestureEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class GesturesRotateDisplayObject extends Sprite 
	{
		private var box:Shape;
		public function GesturesRotateDisplayObject() 
		{
			init();
		}
		
		private function init():void 
		{
			setupBox();
			
			
			setupTouchEvent();
		}
		
		private function setupTouchEvent():void 
		{
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			stage.addEventListener(TransformGestureEvent.GESTURE_ROTATE, onRotate);
		}
		
		private function onRotate(e:TransformGestureEvent):void 
		{
			box.rotation += e.rotation;
		}
		
		private function setupBox():void 
		{
			box = new Shape();
			box.graphics.beginFill(0xff5544, 0.5);
			box.x = stage.stageWidth / 2;
			box.y = stage.stageHeight / 2;
			box.graphics.drawRect( -100, -100, 200, 200);
			box.graphics.endFill();
			addChild(box);
			
		}
		
	}

}
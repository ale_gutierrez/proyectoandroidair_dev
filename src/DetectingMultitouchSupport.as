package 
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Multitouch;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class DetectingMultitouchSupport extends Sprite 
	{
		
		private var traceField:TextField;
		private var traceFormat:TextFormat;
		
		public function DetectingMultitouchSupport() 
		{
			init();
		}
		
		private function init():void 
		{
			setupTextField();
			
			checkMultitouchEvent();
		}
		
		private function checkMultitouchEvent():void 
		{
			traceField.appendText("Gestures : " + Multitouch.supportedGestures + "\n") ;
			traceField.appendText("Touch : " + Multitouch.supportsTouchEvents + "\n") ;
		}
		
		private function setupTextField():void 
		{
			traceFormat = new TextFormat();
			traceFormat.font = '_sans';
			traceFormat.size = 12;
			traceFormat.bold = true;
			traceFormat.color = 0xff0055;
			traceFormat.align = 'center';
			
			traceField = new TextField();
			traceField.defaultTextFormat = traceFormat;
			traceField.selectable = false;
			traceField.mouseEnabled = false;
			traceField.width = stage.stageWidth;
			traceField.height = stage.stageHeight;
			
			addChild(traceField);
			
		}
		
	}

}
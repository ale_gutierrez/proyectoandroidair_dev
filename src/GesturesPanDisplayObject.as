package 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.TransformGestureEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class GesturesPanDisplayObject extends Sprite 
	{
		private var box:Shape;
		public function GesturesPanDisplayObject() 
		{
			init();
		}
		
		private function init():void 
		{
			setupBox();
			
			setupTouchEvents();
		}
		
		private function setupTouchEvents():void 
		{
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			stage.addEventListener(TransformGestureEvent.GESTURE_PAN, onPan);
		}
		
		private function onPan(e:TransformGestureEvent):void 
		{
			box.x += e.offsetX;
			box.y += e.offsetY;
		}
		
		private function setupBox():void 
		{
			box = new Shape();
			box.graphics.beginFill(0xff00ff, 0.8);
			box.x = stage.stageWidth / 2;
			box.y = stage.stageHeight / 2;
			box.graphics.drawRect(-100, -100, 200, 200);
			box.graphics.endFill();
			
			box.graphics.lineStyle(10, 0x553311, 1);
			box.graphics.moveTo(0, -800);
			box.graphics.lineTo(0, 800);
			box.graphics.moveTo( -800, 0);
			box.graphics.lineTo(800, 0);
			
			addChild(box);
		}
	}

}
package 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.TransformGestureEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class GesturesSwipeDisplayObject extends Sprite 
	{
		private var box:Shape;
		
		public function GesturesSwipeDisplayObject() 
		{
			init();
		}
		
		private function init():void 
		{
			setupBox();
			setupTouchevents();
		}
		
		private function setupTouchevents():void 
		{
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			stage.addEventListener(TransformGestureEvent.GESTURE_SWIPE, onSwipe);
		}
		
		private function onSwipe(e:TransformGestureEvent):void 
		{
			switch(e.offsetX){
				case 1:{
					box.x = stage.stageWidth - (box.width / 2);
					break;
				}
				case -1:{
					box.x = box.width / 2;
					break;
				}
			}
			switch(e.offsetY){
				case 1:{
					box.y = stage.stageHeight -(box.height / 2);
					break;
				}
				case -1:{
					box.y = box.height / 2;
					break;
				}
			}
			
		}
		
		private function setupBox():void 
		{
			box = new Shape();
			box.graphics.beginFill(0x00ff66, 0.7);
			box.x = stage.stageWidth / 2;
			box.y = stage.stageHeight / 2;
			box.graphics.drawRect( -100, -100, 200, 200);
			box.graphics.endFill();
			addChild(box);
		}
		
	}

}
package 
{
	import flash.display.Sprite;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	
	/**
	 * ...
	 * @author Alejandro
	 */
	public class DetectingSupportDeviceInput extends Sprite 
	{
		// variable de salida
		private var traceField:TextField;
		private var traceFormat:TextFormat;
		public function DetectingSupportDeviceInput() 
		{
			init();
		}
		
		private function init():void 
		{
			setupTextField();
			checkInputTypes();
		}
		
		private function checkInputTypes():void 
		{
			traceField.appendText("Touch Sceen Type : " + Capabilities.touchscreenType + "\n");
			traceField.appendText("Mouse Cursor : " + Mouse.supportsCursor + "\n");
			traceField.appendText("Teclado Fisico : " + Keyboard.physicalKeyboardType + "\n");
			traceField.appendText("teclado Virtual : " + Keyboard.hasVirtualKeyboard + "\n");
		}
		
		private function setupTextField():void 
		{
			traceFormat = new TextFormat();
			traceFormat.bold = true;
			traceFormat.font = '_sans';
			traceFormat.size = 12;
			traceFormat.align = 'center';
			traceFormat.color = '0xff5500';
			
			
			traceField = new TextField();
			traceField.defaultTextFormat = traceFormat;
			traceField.selectable = false;
			traceField.mouseEnabled = false;
			traceField.width = stage.stageWidth;
			traceField.height = stage.stageHeight;
			addChild(traceField);
			
		}
		
	}
}